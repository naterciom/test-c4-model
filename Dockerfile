FROM amazoncorretto:11-alpine

RUN curl -fsSL https://deb.nodesource.com/setup_18.x | bash -

RUN apt-get update && apt-get install -y nodejs graphviz chromium xvfb jq

RUN npm i -g c4builder
